import React from "react";
import {Image, Text, TouchableOpacity, View} from "react-native";
import colors from "../colors";
import fonts from "../fonts";
import imgPlaceholder from "../../assets/images/account.png";
import date from 'date-and-time';


const MessageItem = ({item, margin, onPress}) => {
    const {
        containerStyle,
        imageStyle,
        productViewStyle,
        nameTextStyle,
        productDetailsViewStyle,
        productDetailsSubviewStyle,
        subviewValueTextStyle,
        separatorStyle,
        subviewValueTextStyle2
    } = styles;

    return (
        <View style={{marginTop: margin / 2, marginBottom: margin / 2}}>
            <TouchableOpacity style={containerStyle}>
                <Image
                    style={[{ marginRight: margin }, imageStyle]}
                    source={imgPlaceholder}
                />

                <View style={productViewStyle}>

                    <View style={productDetailsViewStyle}>
                        <View>
                            <Text style={nameTextStyle}>{item.address}</Text>
                        </View>

                        <View>
                            <Text style={subviewValueTextStyle}>{date.format(new Date(item.date), 'DD MMM')}</Text>
                        </View>
                    </View>

                    <View style={productDetailsViewStyle}>
                        <View style={productDetailsSubviewStyle}>
                            <Text numberOfLines={2} style={ item.read === 1 ? subviewValueTextStyle : subviewValueTextStyle2}>{item.body}</Text>
                        </View>
                    </View>

                </View>
            </TouchableOpacity>

            <View style={[{marginTop: margin}, separatorStyle]}/>
        </View>
    );
};

const higherMargin = 5;
const lowerMargin = 2.5;
const styles = {
    containerStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    imageStyle: {
        backgroundColor: colors.grayUltraLight,
        width: 40,
        height: 40,
        borderRadius: 5,
    },

    productViewStyle: {
        flex: 1,
        justifyContent: 'center'
    },
    nameTextStyle: {
        fontFamily: fonts.regular,
        color: colors.grayDark,
        fontSize: 18,
        marginBottom: higherMargin
    },
    productDetailsViewStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent:'space-between'
    },
    productDetailsSubviewStyle: {
        flex: 1
    },
    subviewTitleTextStyle: {
        fontFamily: fonts.light,
        color: colors.gray,
        fontSize: 14,
        marginBottom: lowerMargin
    },
    subviewValueTextStyle: {
        fontFamily: fonts.regular,
        color: colors.gray,
        fontSize: 12,
        height:30
    },
    subviewValueTextStyle2: {
        fontFamily: fonts.bold,
        color: colors.black,
        fontSize: 12,
        height:30
    },

    separatorStyle: {
        backgroundColor: colors.grayLight,
        height: 0.5
    }
};


export {MessageItem};
