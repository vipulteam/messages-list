import {AsyncStorage} from "react-native";
class AuthenticationService {

    static loginInUser(user, password) {
        console.log(user);
        console.log(password);
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(user)
                .then((passwordUser) => {
                    if (passwordUser == password) {
                        resolve();
                    } else {
                        reject('Invalid Username/password');
                    }
                })
                .catch((error) => {
                    reject('Invalid Username/password');
                });
        });
    }

    static signUpUser(user) {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(user)
                .then((passwordUser) => {
                    const token = JSON.parse(passwordUser);
                    if (token == null) {
                        resolve();
                    } else {
                        reject('User already exists');
                    }
                })
                .catch(() => reject('User already exists'));
        });

    }

}

export {AuthenticationService};
