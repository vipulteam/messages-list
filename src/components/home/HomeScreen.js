import React, {Component} from "react";
import {AsyncStorage, FlatList, PermissionsAndroid, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {messagesFetch} from "../../actions/HomeActions";
import {LoadingView} from "../../views/LoadingView";
import {MessageItem} from "../../views/MessageItem";
import {NoContentView} from "../../views/NoContentView";
import colors from "../../colors";
import fonts from "../../fonts";
import i18n from "../../i18n";
import imgAppCloud from "../../../assets/images/app-cloud.png";


type Props = {};
class HomeScreen extends Component<Props> {

    state = {
        messagePermission: false,
    }
    static navigationOptions = ({navigation}) => {
        const {
            buttonSytle
        } = styles;

        return {
            headerTitle: 'Messages List',
            headerRight: (
                <TouchableOpacity style={buttonSytle} onPress={navigation.getParam('signOut')}>
                    <Text style={buttonSytle}>Sign Out</Text>
                </TouchableOpacity>
            ),
        };
    };

    async componentWillMount() {
        await this.requestSMSPermission();
        if (this.state.messagePermission) {
            this.props.messagesFetch();
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({signOut: this._signOutAsync});
    }

    componentWillUnmount() {
        this.props.messages = [];
    }


    renderLoading() {
        return (
            <LoadingView />
        );
    }

    renderNoContent() {
        return (
            <NoContentView
                image={imgAppCloud}
                title={i18n.t('messages.noContent')}
            />
        );
    }

    renderNoPermission() {
        return (
            <NoContentView
                image={imgAppCloud}
                title={i18n.t('messages.noContent')}
            />
        );
    }

    renderList() {
        const {
            containerStyle,
            flatListContainerStyle,
        } = styles;

        return (
            <View style={containerStyle}>
                <FlatList
                    contentContainerStyle={flatListContainerStyle}
                    data={this.props.messages}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item}) =>
                        <MessageItem
                            margin={padding}
                            item={item}
                            onPress={() => Actions.message({message: item})}
                        />
                    }
                />
            </View>
        );
    }

    render() {
        if (this.state.messagePermission) {
            if (this.props.loading) {
                return this.renderLoading();
            }

            if (this.props.messages.length === 0) {
                return this.renderNoContent();
            }


            return this.renderList();
        } else {
            return this.renderNoPermission();
        }


    }

    _signOutAsync = async () => {
        await AsyncStorage.removeItem('userToken');
        this.props.navigation.navigate('Auth');
    };


    async requestSMSPermission() {
        try {
            const granted = await
                PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_SMS
                )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("granted")
                this.setState({'messagePermission': true});
            } else {
                console.log("not granted")
                this.setState({'messagePermission': false});
            }
        } catch (err) {
            console.warn(err)
        }
    }
}


const padding = 14;
export const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: colors.white
    },
    flatListContainerStyle: {
        paddingTop: padding / 2,
        paddingBottom: padding / 2,
        paddingLeft: padding,
        paddingRight: padding
    },
    buttonSytle: {
        fontFamily: fonts.bold,
        color: colors.black,
        fontSize: 16,
        height: 30,
        marginRight: 10,
        marginTop: 4
    },
});

const mapStateToProps = state => {
    const {loading, messages} = state.messages;
    return {loading, messages};
};

export default connect(mapStateToProps, {
    messagesFetch
})(HomeScreen);
