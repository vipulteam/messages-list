import React, {Component} from "react";
import {Button, StyleSheet, Text, View} from "react-native";
import {TextField} from "react-native-material-textfield";
import {signUpUser} from "../../../actions/LoginActions";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";


type Props = {};
class SignUpScreen extends React.Component<Props> {

    state = {
        username: '',
        password: '',
        confirmPassword: '',
        validUsername: false,
        validPassword: false,
        validConfirmPassword: false,
        usernameError: 'Invalid Email',
        passwordError: 'Invalid Password',
        confirmPasswordError: 'Password and confirm password should be same',
        isSubmitButtonEnabled: false
    };
    static navigationOptions = {
        title: 'Sign Up',
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.success) {
            const nav = NavigationActions.navigate({
                routeName: 'App',
            });
            this.props.navigation.dispatch(nav);
        }
    }

    handleSubmit = async () => {
        this.props.signUpUser(this.state.username, this.state.password);
    };

    validateUsername = (text) => {
        this.state.username = text;
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            this.setState({'validUsername': true}, () => {
                this.setButtonstate();
            });
            this.setState({'userNameError': 'Invalid Email'}, () => {
            });
        }
        else {
            this.setState({'validUsername': false}, () => {
                this.setButtonstate();
            });
            this.setState({'userNameError': 'Valid Email'}, () => {
            });
        }
    }

    validatePassword = (text) => {
        this.state.password = text;
        if (text.length == 0) {

            this.setState({'validPassword': true}, () => {
                this.setButtonstate();
            });
            this.setState({'passwordError': 'Password should not be empty'}, () => {
            });
        } else if (text != null && text.length < 6) {
            this.setState({'validPassword': true}, () => {
                this.setButtonstate();
            });
            this.setState({'passwordError': 'Password should be atleast 6 characters long'}, () => {
            });
        } else if (text != null && text.length > 10) {
            this.setState({'validPassword': true}, () => {
                this.setButtonstate();
            });
            this.setState({'passwordError': 'Password should be less than 10 characters long'}, () => {
            });
        } else if (text != null && this.state.confirmPassword.length > 0) {
            if (text != this.state.confirmPassword) {
                this.setState({'validPassword': true}, () => {
                    this.setButtonstate();
                });
                this.setState({'passwordError': 'Password and ConfirmPassword should be same'}, () => {
                });
            } else if (text == this.state.confirmPassword) {
                this.setState({'validPassword': false}, () => {
                    this.setButtonstate();
                    this.setState({'passwordError': 'Password is correct'}, () => {
                    });
                });
                this.setState({'validConfirmPassword': false}, () => {
                    this.setButtonstate();
                    this.setState({'confirmPasswordError': 'Password and Confirm Password matched'}, () => {
                    });
                });

            }
        } else {
            if (text != null) {
                this.setState({'validPassword': false}, () => {
                    this.setButtonstate();
                });
                this.setState({'passwordError': 'Password is correct'}, () => {
                });
            }

        }

    }

    validateConfirmPassword = (text) => {
        if (text != null)
            this.state.confirmPassword = text;
        if (text != null && text.length == 0) {
            this.setState({'validConfirmPassword': true}, () => {
                this.setButtonstate();
            });
            this.setState({'confirmPasswordError': 'Confirm Password should not be empty'}, () => {
            });
        } else if (text != null && this.state.password.length > 0) {
            if (text != this.state.password) {
                this.setState({'validConfirmPassword': true}, () => {
                    this.setButtonstate();
                });
                this.setState({'confirmPasswordError': 'Password and ConfirmPassword should be same'}, () => {
                });
            } else if (text == this.state.password && this.state.password.length <= 10) {
                this.setState({'validPassword': false}, () => {
                    this.setButtonstate();
                    this.setState({'passwordError': 'Password is correct'}, () => {
                    });
                });
                this.setState({'validConfirmPassword': false}, () => {
                    this.setButtonstate();
                    this.setState({'confirmPasswordError': 'Password and Confirm Password matched'}, () => {
                    });
                });

            }
        } else {
            this.setState({'validConfirmPassword': false}, () => {
                this.setButtonstate();
            });
            this.setState({'confirmPasswordError': 'Password and Confirm Password matched'}, () => {
            });
        }

    }

    setButtonstate() {
        this.setState({'isSubmitButtonEnabled': !this.state.validPassword && !this.state.validUsername && !this.state.validConfirmPassword && this.state.confirmPassword.length > 0 && this.state.password.length > 0 && this.state.username.length > 0})
    }


    render() {
        return (
            <View style={styles.container}>
                <TextField
                    label="Username"
                    onChangeText={(text) => this.validateUsername(text)}
                    value={this.state.username}
                />
                {this.state.validUsername && <Text style={{color: 'red'}}>{this.state.usernameError}</Text>}
                <TextField
                    label="Password"
                    secureTextEntry={true}
                    onChangeText={(value) => {
                        this.validatePassword(value);
                    }}
                    value={this.state.password}
                />
                {this.state.validPassword && <Text style={{color: 'red'}}>{this.state.passwordError}</Text>}
                <TextField
                    label="Confirm Password"
                    secureTextEntry={true}
                    onChangeText={(value) => {
                        this.validateConfirmPassword(value);
                    }}
                    value={this.state.confirmPassword}
                />
                {this.state.validConfirmPassword &&
                <Text style={{color: 'red'}}>{this.state.confirmPasswordError}</Text>}
                <Text style={styles.text}>By signing up,you agree to our Terms & Conditions & Privacy Policy.</Text>
                <View style={{marginTop: 30}}>
                    <Button
                        title="Sign Up"
                        disabled={!this.state.isSubmitButtonEnabled}
                        onPress={this.handleSubmit}
                    />
                </View>
            </View>
        );
    }
}

const
    styles = StyleSheet.create({
        container: {
            justifyContent: 'center',
            marginLeft: 15,
            marginRight: 15

        },
        inputField: {
            marginTop: 30,
            fontSize: 20,
            marginLeft: 15,
            marginRight: 15,
        },
        text: {
            marginTop: 30,
            marginLeft: 15,
            marginRight: 15,
            fontSize: 15,
        }
    });

const
    mapStateToProps = state => {
        return {
            user: state.login.user,
            loading: state.login.loading,
            success: state.login.success
        }
    };

export default connect(mapStateToProps, {
    signUpUser
})(SignUpScreen);

