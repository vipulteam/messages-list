import React from "react";
import {ActivityIndicator, AsyncStorage, StatusBar, StyleSheet, View, Image} from "react-native";
import imgPlaceholder from "../../../assets/images/onthego.jpg";



export class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('userToken');
        setTimeout(() => {
            this.props.navigation.navigate(userToken ? 'App' : 'Auth');
        }, 2000);
    };

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
                <Image
                    source={imgPlaceholder}
                />
                <StatusBar barStyle="default"/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:'#000'
    },
});
