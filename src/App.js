import React, {Component} from "react";
import {StyleSheet} from "react-native";
import HomeReducer from "./reducers/HomeReducer";
import LoginReducer from "./reducers/LoginReducer";
import {
    createAppContainer,
    createMaterialTopTabNavigator,
    createStackNavigator,
    createSwitchNavigator
} from "react-navigation";
import {AuthLoadingScreen} from "./components/authentication/AuthLoadingScreen";
import SignUpScreen from "./components/authentication/SignUp/SignUpScreen";
import SignInScreen from "./components/authentication/SignIn/SignInScreen";
import HomeScreen from "./components/home/HomeScreen";
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, createStore} from "redux";
import {reducer as formReducer} from "redux-form";
import ReduxThunk from "redux-thunk";

const rootReducer = combineReducers({
    form: formReducer,
    messages: HomeReducer,
    login: LoginReducer
});
const store = createStore(rootReducer, {}, applyMiddleware(ReduxThunk));


const AppStack = createStackNavigator({Home: HomeScreen});
const AuthStack = createMaterialTopTabNavigator({
    SignIn: SignInScreen,
    SignUp: SignUpScreen,
});

const AppContainer = createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack,

    },
    {
        initialRouteName: 'AuthLoading',
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#f4511e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        },
    }
));
type Props = {};

export default class App extends Component<Props> {
    render() {
        return (
            <Provider store={store}>
                <AppContainer />
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
