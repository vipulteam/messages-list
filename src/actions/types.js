export const LOGIN = 'loginUser';
export const LOGIN_FAIL = 'login_fail';
export const LOGIN_SUCCESS = 'login_success';

export const SIGNUP_SUCCESS = 'signup_success';
export const SIGNUP_FAIL ='signup_fail';
export const SIGNUP = 'signup';

export const MESSAGES_FETCH = 'messgaes_fetch';
export const MESSAGES_FETCH_SUCCESS = 'messgaes_fetch_success';

