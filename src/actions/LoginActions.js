import {Alert, AsyncStorage} from "react-native";
import {LOGIN, LOGIN_FAIL, LOGIN_SUCCESS, SIGNUP, SIGNUP_FAIL, SIGNUP_SUCCESS} from "./types";
import i18n from "../i18n";
import {AuthenticationService} from "../services/authenticationService";


export const logInUser = (user, password) => {
    return (dispatch) => {
        dispatch({type: LOGIN});
        AuthenticationService.loginInUser(user, password)
            .then(() => {
                AsyncStorage.setItem('userToken', user)
                    .then(() => {
                        loginSuccess(dispatch, user);
                    })
                    .catch(error => {
                        loginFail(dispatch, error);
                    });
            })
            .catch(error => {
                loginFail(dispatch, error);
            });


    };
}

export const signUpUser = (user, password) => {
    return (dispatch) => {
        dispatch({type: SIGNUP});
        AuthenticationService.signUpUser(user)
            .then(() => {
                AsyncStorage.setItem(user, password)
                    .then(() => {
                        AsyncStorage.setItem('userToken', user)
                            .then(() => {
                                    signUpSuccess(dispatch, 'success')
                                }
                            ).catch(error => {
                            signupFail(dispatch, error);
                        })
                    })
                    .catch(error => {
                        signupFail(dispatch, error);
                    });
            })
            .catch(error => {
                signupFail(dispatch, error)
            });


    };
}

export const signUpSuccess = (dispatch, user) => {
    dispatch({type: SIGNUP_SUCCESS, payload: user});
}

export const signupFail = (dispatch, error) => {
    dispatch({type: SIGNUP_FAIL});

    if (error) {
        Alert.alert(
            i18n.t('app.attention'),
            i18n.t('signup.enter.message'),
            [{text: i18n.t('app.ok')}],
            {cancelable: true}
        );
    }
}

export const loginSuccess = (dispatch, user) => {
    dispatch({type: LOGIN_SUCCESS, payload: user});

};

export const loginFail = (dispatch, error) => {
    dispatch({type: LOGIN_FAIL});

    if (error) {
        Alert.alert(
            i18n.t('app.attention'),
            i18n.t('login.enter.message'),
            [{text: i18n.t('app.ok')}],
            {cancelable: true}
        );
    }
};
