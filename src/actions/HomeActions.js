import { MESSAGES_FETCH, MESSAGES_FETCH_SUCCESS } from './types';
import SmsAndroid  from 'react-native-get-sms-android';

var filter = {
    box: 'inbox', // 'inbox' (default), 'sent', 'draft', 'outbox', 'failed', 'queued', and '' for all
    indexFrom: 0, // start from index 0
    maxCount: 100, // count of SMS to return each time
};

export const messagesFetch = () => {
        return (dispatch) => {
            dispatch({ type: MESSAGES_FETCH });

            const unsubscribe = SmsAndroid.list(JSON.stringify(filter), (fail) => {
                    console.log("Failed with this error: " + fail)
                },
                (count, smsList) => {
                    var messages = [];

                    messages = JSON.parse(smsList);
                    dispatch({
                        type: MESSAGES_FETCH_SUCCESS,
                        payload: { messages }
                    });
                });
        };
};

