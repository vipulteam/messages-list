import {LOGIN, LOGIN_FAIL, LOGIN_SUCCESS, SIGNUP, SIGNUP_FAIL, SIGNUP_SUCCESS} from "../actions/types";
import i18n from "../i18n";

const INITIAL_STATE = {
    loading: false,
    user: '',
    errorMessage: '',
    success: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...INITIAL_STATE,
                loading: true,
                success: false
            };

        case LOGIN_FAIL:
            return {
                ...INITIAL_STATE,
                loading: false,
                success: false,
                errorMessage: i18n.t('login.enter.message')
            };

        case LOGIN_SUCCESS:
            return {
                ...INITIAL_STATE,
                user: action.payload.user,
                loading: false,
                success: true
            };
        case SIGNUP:
            return {
                ...INITIAL_STATE,
                loading: true,
                success: false
            };

        case SIGNUP_FAIL:
            return {
                ...INITIAL_STATE,
                loading: false,
                success: false,
                errorMessage: i18n.t('signup.enter.message')
            };

        case SIGNUP_SUCCESS:
            return {
                ...INITIAL_STATE,
                user: action.payload.user,
                loading: false,
                success: true
            };
        default:
            return state;
    }
};
