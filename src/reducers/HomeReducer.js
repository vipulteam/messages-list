import { MESSAGES_FETCH, MESSAGES_FETCH_SUCCESS } from '../actions/types';

const INITIAL_STATE = {
    loading: true,
    messages: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case MESSAGES_FETCH:
            return INITIAL_STATE;

        case MESSAGES_FETCH_SUCCESS:
            return {
                loading: false,
                messages: action.payload.messages
            };

        default:
            return state;
    }
};
